<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{   public function tambah() {
        return view('cast.create');
    }   

    public function simpan(Request $request) {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required',
        ]);
        
        DB::table('cast')->insert([
            'nama'=>$request['nama'],
            'umur'=>$request['umur'],
            'bio'=>$request['bio']            
        ]);
        
        return redirect('/cast');
    }   

    public function index(){
        $cast = DB::table('cast')->get();
        return view ('cast.index', compact('cast'));
    }

    public function lihat($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view ('cast.lihat', compact('cast'));
    }

    public function ubah($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view ('cast.ubah', compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required',
        ]);

        $query = DB::table('cast')
            ->where('id',$id)
            ->update([
                'nama'=>$request['nama'],
                'umur'=>$request['umur'],
                'bio'=>$request['bio']  
            ]);

        return redirect('/cast');
    }

        public function hapus($id){
            DB::table('cast')->where('id',$id)->delete();
            return redirect('/cast');
        

    }
}
