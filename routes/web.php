<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');

Route::get('/data', 'AuthController@formulir'); 

Route::post('/kirim', 'AuthController@kirimdata'); 

Route::get('/table', function() {
    return view('table/table');
});

Route::get('/data-table', function() {
    return view('table/datatable');
});

Route::get('/cast/create', 'CastController@tambah'); 
Route::post('/cast', 'CastController@simpan'); 
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@lihat');  
Route::get('/cast/{cast_id}/ubah', 'CastController@ubah'); 
Route::put('/cast/{cast_id}', 'CastController@update');  
Route::delete('/cast/{cast_id}', 'CastController@hapus');  

