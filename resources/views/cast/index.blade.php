@extends('layout.utama')

@section('judul')
Halaman LIHAT DATA CAST
@endsection

@section('content')


<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td> 
                            <form action="/cast/{{$value->id}}" method="post">
                                @method('delete')
                                @csrf
                                <a href="/cast/{{$value->id}}" class="btn btn-info ">Detail</a>
                                <a href="/cast/{{$value->id}}/ubah" class="btn btn-primary ">Edit</a>

                                <input type="submit" class="btn btn-danger " value="Delete">
                            </form>
                            
                        </td>
                    </tr>
                @empty
                <tr>
                    <td>Tidak Ada data</td>
                </tr>
                        

                @endforelse              
            </tbody>
        </table>

@endsection