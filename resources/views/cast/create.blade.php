@extends('layout.utama')

@section('judul')
Halaman CREATE DATA
@endsection

@section('content')

    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label >Nama</label>
            <input type="text" name="nama" class="form-control" >
            @error('nama')
                <div class="alert alert-danger">
                    {{$message}}
                </div>                
            @enderror
        </div>

        <div class="form-group">
            <label >Umur</label>
            <input type="number" name="umur" class="form-control" >
            @error('umur')
            <div class="alert alert-danger">
                {{$message}}
            </div>                
        @enderror
        </div>
        
        <div class="form-group">
            <label >Biodata</label>
            <textarea name="bio" class="form-control" cols="30" rows="5"></textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{$message}}
            </div>                
        @enderror

        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection