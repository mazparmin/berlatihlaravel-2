@extends('layout.utama')

@section('judul')
Halaman UBAH DATA CAST  {{$cast->nama}}
@endsection

@section('content')

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label >Nama</label>
            <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
            @error('nama')
                <div class="alert alert-danger">
                    {{$message}}
                </div>                
            @enderror
        </div>

        <div class="form-group">
            <label >Umur</label>
            <input type="number" name="umur" value={{$cast->umur}} class="form-control" >
            @error('umur')
            <div class="alert alert-danger">
                {{$message}}
            </div>                
        @enderror
        </div>
        
        <div class="form-group">
            <label >Biodata</label>
            <textarea name="bio"  class="form-control" cols="30" rows="5" >{{$cast->bio}} </textarea>
            @error('bio')
            <div class="alert alert-danger">
                {{$message}}
            </div>                
        @enderror

        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

@endsection